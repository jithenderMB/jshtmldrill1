// Task 1

let getSectionUsingId =  document.getElementById('container'); 

// Task 2
let getSectionUsingQuerySelector = document.querySelector('#container');

// Task 3
let getAllElementsOfClassNameSecond = document.querySelectorAll('.second');

// Task 4
let getClassThirdFromOlTag = document.getElementsByTagName('ol')[0].querySelector('.third'); 

// Task 5
getSectionUsingId.append = 'Hello!';

// Task 6
let getClassFooter = document.querySelector('.footer');
let createDivElement = document.createElement('div');
createDivElement.classList.add('main'); // add class main to the create div element
getClassFooter.appendChild(createDivElement); // or getClassFooter.innerHTML = '<div class='main'></div>'

// Task 7
getClassFooter.removeChild(createDivElement);  //or getClassFooter.textContent = '';

// Task 8
let createLiElement = document.createElement('li');

// Task 9
createLiElement.textContent = 'four';

// Task 10
let getUlElement = document.getElementsByTagName('ul')[0];
getUlElement.appendChild(createLiElement);

// Task 11
let getLiElementsFromOlElement = document.querySelector('ol').querySelectorAll('li');
for (let element of getLiElementsFromOlElement){
    element.style.color = 'green';
} 

// Task 12
let DivElementWithClassFooter = document.querySelector('.footer')

DivElementWithClassFooter.remove();

